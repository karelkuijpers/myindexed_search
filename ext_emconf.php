<?php

########################################################################
# Extension Manager/Repository config file for ext: "myfile_list"
#
#
# Manual updates:
# Only the data in the array - anything else is removed by next write.
# "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Customization of indexed_search extension',
	'description' => 'This extension adapts the indexed search.',
	'version' => '11.0.1',
	'shy' => 0,
	'dependencies' => 'indexed_search',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => '',
	'state' => 'stable',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 1,
	'lockType' => '',
	'author' => 'Karel Kuijpers',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'constraints' => array(
		'depends' => array(
	            'typo3' => '8.7.13-11.5.99',
	        ),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'autoload' => 
  		array(
    	'classmap' => array('Classes')
  	),
);

?>